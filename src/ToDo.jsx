import React, { Component } from "react";
import "./ToDoList.css";
import FormField from "./components/Form";
import TaskToDo from "./components/task";
import CompleteTask from "./components/CompleteTask";
import { connect } from "react-redux";
import { moveToDo } from "./actions";

class ToDoList extends Component {
  state = {
    taskData: ""
  };

  onDragStart = (event, id) => {
    event.dataTransfer.setData("id", id);
  };

  onDrop = event => {
    let id = event.dataTransfer.getData("id");
    this.props.dispatch(moveToDo(id));
  };

  onDragOver = event => {
    event.preventDefault();
  };

  handleChange = ({ target }) => {
    this.setState({ taskData: target.value });
  };

  render() {
    return (
      <div className="container-drag">
        <FormField
          taskData={this.state.taskData}
          handleChange={this.handleChange}
        />
        <TaskToDo
          onDragOver={this.onDragOver}
          list={this.props.taskArray}
          onDragStart={this.onDragStart}
        />

        <CompleteTask
          onDragOver={this.onDragOver}
          onDrop={this.onDrop}
          taskDone={this.props.taskArray}
          onDragStart={this.onDragStart}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  taskArray: state.tasks
});

export default connect(mapStateToProps)(ToDoList);
