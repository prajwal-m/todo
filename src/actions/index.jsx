export const addToDo = () => {
  return {
    type: "ADD_TASK"
  };
};

export const removeToDo = index => {
  return { type: "REMOVE_TASK", keyID: index };
};

export const moveToDo = id => {
  return { type: "MOVE_TASK", key: id };
};
