import React from "react";
import "./form.css";
import { useDispatch } from "react-redux";

const ButtonComponent = props => {
  const dispatch = useDispatch();
  return (
    <button type="submit" onClick={() => dispatch(props.addToDo())}>
      Add
    </button>
  );
};

export default ButtonComponent;
