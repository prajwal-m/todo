import React from "react";
import "./completeTask.css";
import Cards from "../components/cards";

const CompleteTask = ({ onDragOver, onDrop, taskDone, onDragStart }) => {
  const renderCards = (individualTask, index) => (
    <Cards
      index={index}
      key={individualTask.key}
      keyValue={individualTask.key}
      taskName={individualTask.name}
      onDragStart={onDragStart}
    />
  );
  return (
    <div
      className="droppable"
      onDragOver={event => onDragOver(event)}
      onDrop={event => onDrop(event, "complete")}
    >
      <span className="task-header">COMPLETED</span>

      {taskDone
        .filter(elem => elem.category === "complete")
        .map((elem, index) => renderCards(elem, index))}
    </div>
  );
};

export default CompleteTask;
