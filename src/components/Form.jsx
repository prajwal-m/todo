import React from "react";
import "./form.css";
import { addToDo } from "../actions/";
// import { useDispatch } from "react-redux";
import ButtonComponent from "./Button";

const handleSubmit = event => {
  event.preventDefault();
  const inputField = document.querySelector(".header");
  inputField.reset();
};

const FormField = ({ taskData, handleChange }) => {
  // const dispatch = useDispatch();

  return (
    <form onSubmit={handleSubmit} className="header">
      <input
        className="input-field"
        name="taskData"
        value={taskData}
        onChange={handleChange}
      />

      <ButtonComponent addToDo={addToDo} />
    </form>
  );
};

export default FormField;
