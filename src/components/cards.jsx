import React from "react";
import "./cards.css";
import { useDispatch } from "react-redux";
import { removeToDo } from "../actions";

const Cards = ({ index, keyValue, taskName, onDragStart }) => {
  const dispatch = useDispatch();
  return (
    <div
      key={keyValue}
      onDragStart={event => onDragStart(event, keyValue)}
      draggable
      className="draggable"
    >
      {taskName}

      <button
        name="removeTask"
        className="close-btn"
        onClick={() => dispatch(removeToDo(keyValue))}
      >
        x
      </button>
    </div>
  );
};

export default Cards;
