import React from "react";
import "./task.css";
import Cards from "../components/cards";

const TaskToDo = ({ onDragOver, list, onDragStart }) => {
  const renderCards = (individualTask, index) => (
    <Cards
      index={index}
      key={individualTask.key}
      keyValue={individualTask.key}
      taskName={individualTask.name}
      onDragStart={onDragStart}
    />
  );
  return (
    <div className="todo" onDragOver={event => onDragOver(event)}>
      <span className="task-header">TO DO</span>
      {list
        .filter(individualTask => individualTask.category === "todo")
        .map((individualTask, index) => renderCards(individualTask, index))}
    </div>
  );
};

export default TaskToDo;
