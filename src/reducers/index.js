export function reducer(state = { tasks: [] }, action) {
  switch (action.type) {
    case "ADD_TASK":
      const inputData = document.querySelector(".input-field").value;
      console.log(inputData);
      return Object.assign({}, state, {
        tasks: [
          ...state.tasks,
          {
            name: inputData,
            category: "todo",
            key: Math.random()
              .toString(36)
              .substring(7)
          }
        ]
      });

    case "REMOVE_TASK":
      const newList = [...state.tasks];
      const getindex = newList.findIndex(item => item.key === action.keyID);
      console.log("reducer index" + getindex);
      return {
        ...state,
        tasks: [
          ...state.tasks.slice(0, getindex),
          ...state.tasks.slice(getindex + 1)
        ]
      };

    case "MOVE_TASK":
      const list = [...state.tasks];
      console.log(list);
      const getIndex = list.findIndex(i => i.key === action.key);
      console.log(getIndex);
      const newObj = {
        name: list[getIndex].name,
        category: "complete",
        key: action.key
      };
      console.log(newObj);
      return {
        ...state,
        tasks: [
          ...state.tasks.slice(0, getIndex),
          ...state.tasks.slice(getIndex + 1),
          { name: list[getIndex].name, category: "complete", key: action.key }
        ]
      };

    default:
      return state;
  }
}
